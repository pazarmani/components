import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

 //Components
import { DefaultComponent } from './default.component';
import { HomeComponent } from '../pages/home/home.component';
import { ProductsComponent } from '../pages/products/products.component';
import { PromotionsComponent } from '../pages/promotions/promotions.component';
import { DashboardComponent } from '../pages/dashboard/dashboard.component';

 //Modules
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from 'src/app/shared/material-module.module';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    DefaultComponent,
    HomeComponent,
    ProductsComponent,
    DashboardComponent,
    PromotionsComponent
  ],

  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    MaterialModule,
    TranslateModule
  ],

  exports: [
    HomeComponent,
    ProductsComponent,
    DashboardComponent,
    PromotionsComponent
  ],
})

export class DefaultModule { }
