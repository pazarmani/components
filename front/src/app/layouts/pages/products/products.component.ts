import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { Product } from 'src/app/models/product.model';
import { Category } from 'src/app/enums/category.enum';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})

export class ProductsComponent implements OnInit {

  category = Category;
  productos: Product[];

  frutos: Product[];
  semillas: Product[];
  frutas: Product[];
  otros: Product[];
  
  constructor ( private productService: ProductService) {}

  ngOnInit() {
    this.frutos= this.productService.getByCategory(this.category.frutos);
    this.semillas = this.productService.getByCategory(this.category.semillas);
    this.frutas = this.productService.getByCategory(this.category.frutas);
    this.otros = this.productService.getByCategory(this.category.otros);

  }

}