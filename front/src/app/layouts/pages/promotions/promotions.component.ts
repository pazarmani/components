import { Component, OnInit } from '@angular/core';
import { PromotionService } from 'src/app/services/promotion.service';

@Component({
  selector: 'app-promotions',
  templateUrl: './promotions.component.html',
  styleUrls: ['./promotions.component.scss']
})
export class PromotionsComponent implements OnInit {

  monthly_promotion:string;
  month:string;

  constructor( private promotionService: PromotionService) { }

  ngOnInit () {
    this.monthly_promotion = this.promotionService.monthly_promotion;
    this.month = this.promotionService.month;

  }

}
