import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Product } from '../models/product.model';
import { Category } from '../enums/category.enum';

export type UnitType = 'x Kilo' | 'x ½ Kilo' | 'x 1/2 Lt.' | 'x 300 ml/ 250 ml';
 
@Injectable({
  providedIn: 'root'
})

export class ProductService {

   products: Product[] =[
    { 'id': '1', 'description': 'ALMENDRAS peladas Nacional', 'amount': 1260, category:Category.frutos,unitType:'x Kilo'},
    { 'id': '2', 'description': 'ALMENDRAS peladas Non Pareil', 'amount': 1390, category:Category.frutos, unitType:'x Kilo'},
    { 'id': '3', 'description': 'AVELLANAS peladas', 'amount': 1650, category:Category.frutos, unitType:'x Kilo'},
    { 'id': '4', 'description': 'CASTAÑAS naturales', 'amount': 1100, category:Category.frutos, unitType:'x Kilo'},
    { 'id': '5', 'description': 'GRANOLA sin azúcar', 'amount': 330, category:Category.frutos, unitType:'x Kilo'},
    { 'id': '6', 'description': 'GRANOLA especial dulce', 'amount': 420, category:Category.frutos, unitType:'x Kilo'},
    { 'id': '7', 'description': 'MANI sin piel NATURAL', 'amount': 145, category:Category.frutos, unitType:'x Kilo'},
    { 'id': '8', 'description': 'MANI sin piel SALADO', 'amount': 145, category:Category.frutos, unitType:'x Kilo'},
    { 'id': '9', 'description': 'NUECES peladas, CUARTO chandler EXTRA LIGHT', 'amount': 1050, category:Category.frutos, unitType:'x Kilo'},
    { 'id': '10', 'description': 'MIX FRUTOS SECOS C/MANI (almendras, nueces, castañas y maní natural)', 'amount': 780, category:Category.frutos, unitType:'x Kilo'},
    { 'id': '11', 'description': 'MIX FRUTOS SECOS C/PASAS (almendras, nueces, castañas, pasas rubias y morochas)', 'amount': 740, category:Category.frutos, unitType:'x Kilo'},
    { 'id': '12', 'description': 'MIX SALADO (maní salado, pistachos con cáscara, castañas y girasol)', 'amount': 510, category:Category.frutos, unitType:'x Kilo'},
    { 'id': '13', 'description': 'PISTACHOS con cáscara', 'amount': 1500, category:Category.frutos, unitType:'x Kilo'},

    { 'id': '14', 'description': 'GIRASOL pelado', 'amount': 220, category:Category.semillas, unitType:'x Kilo'},
    { 'id': '15', 'description': 'QUINOA', 'amount': 570, category:Category.semillas, unitType:'x Kilo'},
    { 'id': '16', 'description': 'LINO', 'amount': 130, category:Category.semillas, unitType:'x Kilo'},
    { 'id': '17', 'description': 'MIX SEMILLAS (sésamo, chía, lino y girasol)', 'amount': 230, category:Category.semillas, unitType:'x Kilo'},
    { 'id': '18', 'description': 'SESAMO integral', 'amount': 270, category:Category.semillas, unitType:'x Kilo'},
    { 'id': '19', 'description': 'ZAPALLO', 'amount': 850, category:Category.semillas, unitType:'x Kilo'},

    { 'id': '20', 'description': 'ARANDANOS Rojos', 'amount': 880, category:Category.frutas, unitType:'x Kilo'},
    { 'id': '21', 'description': 'ANANÁ rodajas', 'amount': 990, category:Category.frutas, unitType:'x Kilo'},
    { 'id': '22', 'description': 'BANANITAS', 'amount': 620, category:Category.frutas, unitType:'x Kilo'},
    { 'id': '23', 'description': 'CIRUELAS Bombón sin carozo', 'amount': 260, category:Category.frutas, unitType:'x Kilo'},
    { 'id': '24', 'description': 'DAMASCOS', 'amount': 830, category:Category.frutas, unitType:'x Kilo'},
    { 'id': '25', 'description': 'DATILES', 'amount': 360, category:Category.frutas, unitType:'x Kilo'},
    { 'id': '26', 'description': 'HIGOS Negros', 'amount': 490, category:Category.frutas, unitType:'x Kilo'},
    { 'id': '27', 'description': 'PASAS de uva sin semilla MOROCHAS Jumbo', 'amount': 220, category:Category.frutas, unitType:'x Kilo'},
    { 'id': '28', 'description': 'PASAS de uva sin semilla RUBIAS', 'amount': 420, category:Category.frutas, unitType:'x Kilo'},
    { 'id': '29', 'description': 'PERAS', 'amount': 530, category:Category.frutas, unitType:'x Kilo'},

    { 'id': '30', 'description': 'ACEITE COCO', 'amount': 310, category:Category.otros, unitType:'x 300 ml/ 250 ml'},
    { 'id': '31', 'description': 'ACEITE OLIVA', 'amount': 190, category:Category.otros, unitType:'x 1/2 Lt.'},
    { 'id': '32', 'description': 'ALMOHADITAS Frutilla, Avellana o Limón', 'amount': 420, category:Category.otros, unitType:'x Kilo'},
    { 'id': '33', 'description': 'ANILLITOS frutales', 'amount': 330, category:Category.otros, unitType:'x Kilo'},
    { 'id': '34', 'description': 'ARROZ Yamani', 'amount': 125, category:Category.otros, unitType:'x Kilo'},
    { 'id': '35', 'description': 'AVENA cruda o instantánea', 'amount': 110, category:Category.otros, unitType:'x Kilo'},
    { 'id': '36', 'description': 'COCO rallado', 'amount': 420, category:Category.otros, unitType:'x Kilo'},
    { 'id': '37', 'description': 'COPOS con Azúcar', 'amount': 330, category:Category.otros, unitType:'x Kilo'},
    { 'id': '38', 'description': 'COPOS sin Azúcar', 'amount': 270, category:Category.otros, unitType:'x Kilo'},
    { 'id': '39', 'description': 'HARINA Algarroba', 'amount': 170, category:Category.otros, unitType:'x Kilo'},
    { 'id': '40', 'description': 'HARINA Orgánica Integral', 'amount': 90, category:Category.otros, unitType:'x Kilo'},
    { 'id': '41', 'description': 'MASCABO', 'amount': 200, category:Category.otros, unitType:'x Kilo'},
    { 'id': '42', 'description': 'MIEL Pura', 'amount': 120, category:Category.otros, unitType:'x ½ Kilo'},
    { 'id': '43', 'description': 'SAL Marina', 'amount': 145, category:Category.otros, unitType:'x ½ Kilo'},
    { 'id': '44', 'description': 'YERBA Orgánica', 'amount': 230, category:Category.otros, unitType:'x ½ Kilo'},
  ];

  constructor() { }

  getAll(): Product[] {
    return this.products;
  }

  getByCategory(category:Category): Product[] { 
    return this.products.filter(item => item.category === category);
  }
}