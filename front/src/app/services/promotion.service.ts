import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PromotionService {
  
  month: string = '🚨  PROMOCIÓN DE JULIO';
  monthly_promotion: string = 'Azúcar Integral Mascabo: $180 x kilo';
  
  constructor() { }
}
