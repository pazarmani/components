import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HomeService {


  allProducts: string[] =[ 
    'FRUTOS SECOS, SEMILLAS, FRUTAS DESECADAS', 'MIX FRUTOS SECOS C/MANI (almendras, nueces, castañas y maní natural)', 'MIX FRUTOS SECOS C/PASAS (almendras, nueces, castañas, pasas rubias y morochas)', 'MIX SALADO (maní salado, pistachos con cáscara, castañas y girasol)', 'MIX SEMILLAS (sésamo, chía, lino y girasol)', 'ACEITE COCO Y OLIVA, ALMOHADITAS Y ANILLITOS', 'ARROZ Yamani, AVENA, COCO rallado, COPOS', 'HARINA Algarroba y Orgánica Integra, MASCABO', 'MIEL Pura, SAL Marina, YERBA Orgánica'
  ]

  constructor() { }
}
