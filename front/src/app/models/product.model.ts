import { Category } from '../enums/category.enum';
import { UnitType } from '../services/product.service';


export interface Product {
    id: string;
    description: string;
    amount: number;
    unitType: UnitType;
    category:Category
}