import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DefaultComponent } from './layouts/default/default.component';
import { HomeComponent } from './layouts/pages/home/home.component';
import { ProductsComponent } from './layouts/pages/products/products.component';
import { PromotionsComponent } from './layouts/pages/promotions/promotions.component';
import { DashboardComponent } from './layouts/pages/dashboard/dashboard.component';

const routes: Routes = [{
  path: '', component: DefaultComponent,
  children: [{
    path: '',
    component: HomeComponent
  }, {
    path: 'home', component: HomeComponent
  }, {
    path: 'products', component: ProductsComponent
  }, {
    path: 'promotions', component: PromotionsComponent
  }, {
    
    path: '**', pathMatch: 'full', redirectTo: 'home'
  }],
}
];


@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})

export class AppRoutingModule { }
