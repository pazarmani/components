import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule, 
  MatToolbarModule, MatSidenavModule, MatIconModule,
   MatMenuModule, MatListModule, MatTableModule, 
   MatPaginatorModule, MatCardModule, MatFormFieldModule, 
   MatInputModule, MatCheckboxModule, MatSnackBarModule, 
   MatDialogModule, MatProgressBarModule, MatSortModule, 
   MatAutocompleteModule, MatSelectModule, MatProgressSpinnerModule, 
   MatTooltipModule, MatDatepickerModule, MatNativeDateModule, 
   MatRadioModule, MatSlideToggleModule, MatDividerModule, MatGridListModule 
} from '@angular/material';

@NgModule({
  declarations: [],
  exports: [
    CommonModule,
    MatButtonModule,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatMenuModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatSnackBarModule,
    MatDialogModule,
    MatProgressBarModule,
    MatSortModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatSlideToggleModule,
    MatDividerModule,
    MatGridListModule
  
  ],
})
export class MaterialModule { }
