import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Output() toggleSideBarForMe: EventEmitter<any> = new EventEmitter();
  currentLang: string;
  langs: string[]=[];

  constructor(private translate: TranslateService) {
    this.currentLang = this.translate.currentLang;

    this.translate.use('es');
    this.translate.addLangs(['en','es']);
    this.langs = this.translate.getLangs(); 
  }

  onClick(lang: string) {
    this.translate.use(lang);
  }

  ngOnInit() {
  }

  toggleSideBar() {
    this.toggleSideBarForMe.emit();
  }

}

