**Frutos Secos Tandil: https://frutos-secos-tandil.web.app**

Es una web informativa, en desarrollo, que proporciona información sobre un microemprendimiento familiar dedicado a la venta de frutos secos.
> 

	Pre-requisitos de instalación
- Node: 10.16.3  -->  Link para su instalación https://nodejs.org/
- Angular CLI: 7.3.8  -->  npm install -g @angular/cli@7.3.8
> 

	Instalación
- Clone
- npm install
- ng serve -o
> 

	Construido con
- Node: 10.16.3
- Angular CLI: 7.3.8
- Bootstrap 4.5.0
- Angular Material 7.3.7


	i18n:
- ngx-translate
- ngx-translate-extract


	Hosting  -->  https://frutos-secos-tandil.web.app/#/
- Firebase
> 

	En construcción
- Back: Java, Spring, JPA, Hibernate

- Base de Datos: MySql
> 

	Autora
- María de la Paz Armani. https://www.linkedin.com/in/pazarmani/
